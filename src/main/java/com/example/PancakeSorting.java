package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Scanner;

import static java.lang.System.out;

@SpringBootApplication
@RestController
public class PancakeSorting {
    public static void main(String[] args) {
        SpringApplication.run(PancakeSorting.class, args);


        // Call the user input method
//        System.out.println("The sorted array is : ");
//        for (int i : testMethod(userInput()))
//            System.out.print(i + " ");
    }

    @GetMapping("")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }

    public static int[] userInput(){
        // init the scanner class, so we can request user input in the console
        Scanner sc = new Scanner(System.in);

        // reading the number of elements from the user we want to enter
        out.print("Enter the number of elements you want to store, it cannot exceed 25: ");
        int myArrLen = sc.nextInt();

        // check if the number of elements we want to add isn't greater than 25
        while(myArrLen > 25 || myArrLen < 1){
            out.print("You entered a number below 1 or greater than 25, please enter the number of elements again : ");
            myArrLen = sc.nextInt();
        }
        // create the array with the length input from the user
        int[] myArr = new int[myArrLen];

        // reading array elements from the user and adding them to the array
        out.println("Enter the elements of the array: ");
        for(int i=0; i<myArrLen; i++)
        {
            myArr[i] = sc.nextInt();
        }
        return myArr;
    }

    public static int[] testMethod(int[] myArr){
        // call the sorting method with the given array
        pancakeSort(myArr, myArr.length);

        // return the array
        return myArr;
    }

    static void flipArray(int[] myArr, int i) {
        int temp;
        int beg = 0;

        while (beg < i) {
            temp = myArr[beg];
            myArr[beg] = myArr[i];
            myArr[i] = temp;
            beg++;
            i--;
        }
    }
    static int findIndex(int[] myArr, int n) {
        int maxElements;
        int i;
        for (maxElements = 0, i = 0; i < n; ++i)
            if (myArr[i] > myArr[maxElements])
                maxElements = i;
        return maxElements;
    }
    static void pancakeSort(int[] myArr, int n) {
        for (int curr_size = n; curr_size > 1; --curr_size) {
            int maxElements = findIndex(myArr, curr_size);
            if (maxElements != curr_size - 1) {
                flipArray(myArr, maxElements);
                flipArray(myArr, curr_size - 1);
            }
        }
    }
}
