import com.example.PancakeSorting;
import org.junit.jupiter.api.Test;
import org.testng.Assert;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

class PancakeSortingTest {

    @Test
    void pancake_sort() {
        int[] my_arr_actual = { 1, 10, 8, 4, 6, 7, 3, 9, 2, 5 };
        int[] my_arr_expected = {1, 2, 3, 4, 5, 6, 7, 8, 9 ,10};
        int arr_len = 10;

        //1. check if array size is equal to 1 or more (From assignment: The plate always contains a minimum of 1 pancake)
        Assert.assertNotNull(my_arr_actual);

        //2. check if array size doesn't exceed 25 (From assignment: The plate always contains a maximum of 25 pancakes)
        Assert.assertEquals(my_arr_actual.length, 10);

        //3. Test if pancake sort array is equal to the expected array
        // From assignment: sort pancakes by size: the smallest pancake is on top and the largest pancake is at the bottom
        assertTrue(Arrays.deepEquals(new int[][]{PancakeSorting.testMethod(my_arr_actual)}, new int[][]{my_arr_expected}));
    }
}