## Student Information

First Name: Mike

Last Name: Fels

Student number: 500801975

## Assignment 1

### 1. Git log

Used gitkraken instead of git so hereby included an image of the gitkraken repository
![](/images/GitkrakenOutput.JPG)


### 2. Sonarqube

A dated screenshot of the overview of the following  quality gates(https://docs.sonarqube.org/latest/user-guide/quality-gates/): Reliability, Security,Maintainability, Coverage and Duplications. Provide a short discussion of the results.

Before Sonarlint
![](/images/BeforeSonarLint.JPG)  

After Sonarlint
![](/images/AfterSonarLint.JPG)

### 3. Test Driven Development

Your best test class code snippets with a rationale why the unit tests are “good” tests.  Provide a link to the Test class and the class under test in Git.

Had a hard time understanding what this actually means and how to make use of this coding method.
Tried my best by creating a project and testing the outputs based on the user story included in assignment 1


### 4. Code Reviews

Screenshots of the code reviews you have performed on code of another student as comments in Gitlab: Provide a link to the comments in Gitlab.

Made the project at home and wasn't in contact with any of the students so could not review any code

![](/replace-this-with-path-to-your-png-file/screenshot-code-reviews.png) 






